<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 9:00 PM
 */

namespace LaravelUploadHelperImageTreatmentImplementations\FileTreatments;


use Intervention\Image\Facades\Image;
use LaravelUploadHelper\FileTreatments\AbstractFileTreatment;

class HalfSizeImageFileTreatment extends AbstractFileTreatment
{



    public function saveTo(String $fileName, String $savePath)
    {
        $image = Image::make($this->file);
        $height = $image->height();
        $width = $image->width();
        $image->resize($width/2,$height/2);
        $image->save("$savePath/$fileName");
    }


}