<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/13/19
 * Time: 12:34 AM
 */

namespace LaravelUploadHelperImageTreatmentImplementations\FileTreatments;


use Intervention\Image\Facades\Image;
use LaravelUploadHelper\FileTreatments\AbstractFileTreatment;
use LaravelUploadHelper\FileTreatments\StandardFileTreatment;

class SmartPermeableImageTreatment extends StandardFileTreatment
{

    public function saveTo(String $fileName, String $savePath)
    {

        if($this->file->extension() == "svg")
        {
            parent::saveTo($fileName,$savePath);
        }
        else
        {
            $image = Image::make($this->file);
            $image->save("$savePath/$fileName");
        }


    }

}