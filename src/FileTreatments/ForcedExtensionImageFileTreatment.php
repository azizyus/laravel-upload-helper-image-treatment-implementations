<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/13/19
 * Time: 12:44 AM
 */

namespace LaravelUploadHelperImageTreatmentImplementations\FileTreatments;


use Intervention\Image\Facades\Image;
use LaravelUploadHelper\FileNameHelpers\OverrideFileNameExtension;
use LaravelUploadHelper\FileTreatments\StandardFileTreatment;

class ForcedExtensionImageFileTreatment extends StandardFileTreatment
{


    public $fileExtension;
    public $quality;
    public function __construct(String $fileExtension,Int $quality)
    {

        $this->fileExtension = $fileExtension;
        $this->quality = $quality;
    }

    public function saveTo(String $fileName, String $savePath)
    {
        $image = Image::make($this->file)->encode($this->fileExtension,$this->quality);
        $fileName = OverrideFileNameExtension::override($fileName,$this->fileNameExtension,$this->fileExtension);

        $image->save("$savePath/{$fileName}");
    }

}