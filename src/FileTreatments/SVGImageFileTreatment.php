<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/13/19
 * Time: 12:30 AM
 */

namespace LaravelUploadHelperImageTreatmentImplementations\FileTreatments;


use Intervention\Image\Facades\Image;
use LaravelUploadHelper\FileTreatments\StandardFileTreatment;

class SVGImageFileTreatment extends StandardFileTreatment
{

    public function saveTo(String $fileName, String $savePath)
    {
        if($this->file->extension() == "svg")
        {
            parent::saveTo($fileName,$savePath);
        }
        elseif(in_array($this->file->extension(),["jpg","png","jpeg"]))
        {
            $image = Image::make($this->file);
            $image->save("$savePath/$fileName");
        }
        else
        {
            throw new \Exception("the thing you uploaded is not an image");
        }
    }

}