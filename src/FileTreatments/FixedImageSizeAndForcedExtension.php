<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 2/22/19
 * Time: 11:30 PM
 */

namespace LaravelUploadHelperImageTreatmentImplementations\FileTreatments;


use Intervention\Image\Facades\Image;
use LaravelUploadHelper\FileNameHelpers\OverrideFileNameExtension;
use LaravelUploadHelper\FileTreatments\AbstractFileTreatment;

class FixedImageSizeAndForcedExtension extends AbstractFileTreatment
{

    public $height;
    public $width;
    public $quality;
    public $fileExtension;
    public function __construct(String $fileExtension,Int $quality,Int $width,Int $height)
    {

        $this->fileExtension = $fileExtension;
        $this->quality = $quality;
        $this->width  = $width;
        $this->height = $height;

    }

    public function saveTo(String $fileName, String $savePath)
    {
        $image = Image::make($this->file)->encode($this->fileExtension,$this->quality);
        $fileName = OverrideFileNameExtension::override($fileName,$this->fileNameExtension,$this->fileExtension);

        $image->resize($this->width,$this->height,function ($constraint){
            $constraint->aspectRatio();
        });
        $background = Image::canvas($this->width,$this->height);
        $background->insert($image,"center");

        $background->save("$savePath/{$fileName}");

    }

}
