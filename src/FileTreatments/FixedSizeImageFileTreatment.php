<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/12/19
 * Time: 9:00 PM
 */

namespace LaravelUploadHelperImageTreatmentImplementations\FileTreatments;


use Intervention\Image\Facades\Image;
use LaravelUploadHelper\FileTreatments\AbstractFileTreatment;

class FixedSizeImageFileTreatment extends AbstractFileTreatment
{


    public $width;
    public $height;
    public function __construct($width,$height)
    {
        $this->width  = $width;
        $this->height = $height;
    }

    public function saveTo(String $fileName, String $savePath)
    {
        $image = Image::make($this->file);
        $image->resize($this->width,$this->height,function ($constraint){
            $constraint->aspectRatio();
        });
        $background = Image::canvas($this->width,$this->height);
        $background->insert($image,"center");
        $background->save("$savePath/$fileName");
    }


}