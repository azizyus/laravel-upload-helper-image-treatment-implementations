<?php
/**
 * Created by PhpStorm.
 * User: azizyus
 * Date: 1/13/19
 * Time: 12:46 AM
 */

namespace LaravelUploadHelperImageTreatmentImplementations\Enums;

class ImageExtensions
{

    const _JPEG = "jpg";
    const _PNG =  "png";
    const _SVG =  "svg";

}